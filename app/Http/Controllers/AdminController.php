<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class AdminController extends Controller
{
     public function adminindex() 
    {
        return view('admin_home');
    }

     public function task_form()
    {
        $data=DB::table('users')->get(); 
        return view('admin/upload_task', compact('data'));
    }



       public function upload_task(Request $request)
    {
        $this->validate(request(),[
        'Name' => 'required|max:255|min:5',
        'Description'=>'required|max:255',

            //put fields to be validated here
            ]);              
    $task=array();
        $task ['Name']= $request->Name;
         $task ['Contributor_id']= $request->Contributor;
        $task['Category']= $request->Category;
        $task['Description']= $request->Description;

        $current_date_time = date('Y-m-d H:i:s');
        $task['created_at']=$current_date_time;

        $file=$request->file('Task');

        $file_name=hexdec(uniqid());
            $ext=strtolower($file->getClientOriginalExtension());
            $file_full_name=$file_name.'.'.$ext;
            $upload_path='task/';
            $file_url=$upload_path.$file_full_name;
            $success=$file->move($upload_path,$file_full_name);
            $task['Task']=$file_url;

         // echo "<pre>";
         // print_r($task);
         // echo "</pre>";
         // die();
            $data=DB::table('tasks')->insert($task);

        if ($data) {
            $notification = array(
                'message' => 'Data Insert Successful!', 
                'alert-type' => 'success'
                );

            return Redirect()-> route('tasks')->with($notification);
        }
        else{
         $notification = array(
                'message' => 'Data Insert Failed', 
                'alert-type' => 'error'
                );

            return Redirect()-> back()->with($notification);
      }
    }


       public function task_view(){
       // $data=DB::table('tasks')->get();
       $data=DB::table('tasks')->join('users','tasks.Contributor_id','users.id')->select('tasks.*', 'users.name' )->where('Complite',NULL)->get();

       return view('admin/tasks', compact('data'));
    }


     public function complite(){
       $data=DB::table('tasks')->where('Complite',1)->get();

       return view('admin/complite', compact('data'));
    }



     public function complite_task(Request $id)
    {
               
       
    $task=array();
        $task ['Complite']= 1;
        
         // echo "<pre>";
         // print_r($task);
         // echo "</pre>";
         // die();
    $data=DB::table('tasks')->where('id',$id)->update($task);


        if ($data) {
        
            $notification = array(
                'message' => 'Data Complite Successful!', 
                'alert-type' => 'success'
                );

            return Redirect()-> route('tasks')->with($notification);
      }
      else{
         $notification = array(
                'message' => 'Please! try again.', 
                'alert-type' => 'error'
                );

            return Redirect()-> back()->with($notification);
      }
    }



       public function view($id)
    {
        // echo $id;
        // die();
        $data=DB::table('tasks')->where('id',$id)->first();
        return view('admin/viewTask', compact('data'));
    }

    public function edit($id){
      // echo $id;

       $data=DB::table('tasks')->where('id', $id)->first();
       return view('admin/edit', compact('data'));
    }



     public function editdata(Request $request,$id)
    {
        $this->validate(request(),[
        'Name' => 'required|max:255|min:5',
        'Category'=>'required',
        'Description'=>'required',

            //put fields to be validated here
            ]);         
       
    $task=array();
        $task ['Name']= $request->Name;
        $task['Category']= $request->Category;
        $task['Description']= $request->Description;
         // echo "<pre>";
         // print_r($task);
         // echo "</pre>";
         // die();
    $data=DB::table('tasks')->where('id',$id)->update($task);


        if ($data) {
        
            $notification = array(
                'message' => 'Data update Successful!', 
                'alert-type' => 'success'
                );

            return Redirect()-> route('tasks')->with($notification);
      }
      else{
         $notification = array(
                'message' => 'Data Insert Failed', 
                'alert-type' => 'error'
                );

            return Redirect()-> back()->with($notification);
      }
    }

    public function destroy_task($id){

        $task=DB::table('tasks')->where('id',$id)->first();
        $file=$task->Task;

         $data=DB::table('tasks')->where('id',$id)->delete();

         if ($data) {
            unlink($file);
            
            $notification = array(
                'message' => 'Data delete Successful!', 
                'alert-type' => 'success'
                );
            return Redirect()-> route('tasks')->with($notification);
        }  else{
             $notification = array(
                'message' => 'Sorry ! Data delete Failed', 
                'alert-type' => 'error'
                );
            return Redirect()->route('tasks')->with($notification);
        }
        
    }


       public function allusers(){
       $data=DB::table('users')->get();


       return view('admin/allusers', compact('data'));

    }

    public function destroy_usders($id){
         $data=DB::table('users')->where('id',$id)->delete();
         if ($data) {
            
            $notification = array(
                'message' => 'Data delete Successful!', 
                'alert-type' => 'success'
                );
            return Redirect()-> route('allusers')->with($notification);
        }  else{
             $notification = array(
                'message' => 'Sorry ! Data delete Failed', 
                'alert-type' => 'error'
                );
            return Redirect()->route('allusers')->with($notification);
        }
        
    }



    public function stbmited(){
       // $data=DB::table('tasks')->get();
       $data=DB::table('tasks')->join('complite','tasks.id','complite.Task_Id')->select('tasks.*', 'complite.*' )->where('Complite',NULL)->get();

       return view('admin/submited', compact('data'));
    }

    public function downloadtask($Task){

       return response()->download('complite/'.$Task);
    }

  public function reject(){
       $data=DB::table('tasks')->where('Complite',2)->get();

       return view('admin/reject', compact('data'));
    }



     public function reject_task(Request $id)
    {

    $task=array();
        $task ['Complite']= 2;
        
         // echo "<pre>";
         // print_r($task);
         // echo "</pre>";
         // die();
    $data=DB::table('tasks')->where('id',$id)->update($task);


        if ($data) {
        
            $notification = array(
                'message' => 'task Rejected', 
                'alert-type' => 'success'
                );

            return Redirect()-> route('tasks')->with($notification);
      }
      else{
         $notification = array(
                'message' => 'Please! try again.', 
                'alert-type' => 'error'
                );

            return Redirect()-> back()->with($notification);
      }
    }




       public function userView($id)
    {
        // echo $id;
        // die();
        $data=DB::table('users')->where('id',$id)->first();
        return view('admin/user', compact('data'));
    }



}

