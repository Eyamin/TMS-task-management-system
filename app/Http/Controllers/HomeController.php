<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use File;
use Image;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

     public function task_view(){
       $data=DB::table('tasks')->get();

       return view('user/task', compact('data'));
    }


       public function view($id)
    {
        // echo $id;
        // die();
        $data=DB::table('tasks')->where('id',$id)->first();
        return view('user/view', compact('data'));
    }


        public function team_member(){
       $data=DB::table('users')->get();

       return view('user/allusers', compact('data'));
    }


    public function download($task){

       return response()->download('task/'.$task);
    }


     public function Userprofile(){
       $data=DB::table('users')->get();

       return view('user/profile', compact('data'));
    }


    public function profile_picture(Request $request){
        if(
         $request->hasFile('image')){
            $image = $request->file('image');
             $file_name=hexdec(uniqid()).'.'.($image->getClientOriginalExtension());
            Image::make($image)->resize(300, 300)->save(public_path('img/'.$file_name));
            $user=Auth::user();
            $user->image = $file_name;
            $user->save();
        }
        return view('user/profile');
       
    }



   public function submit_task(Request $request)
    {
        $this->validate(request(),[
        'Task_Id' => 'required'

            //put fields to be validated here
            ]);              
    $task=array();
        $task ['Task_Id']= $request->Task_Id;

        $current_date_time = date('Y-m-d H:i:s');
        $task['created_at']=$current_date_time;

        $file=$request->file('Task');

        $file_name=hexdec(uniqid());
            $ext=strtolower($file->getClientOriginalExtension());
            $file_full_name=$file_name.'.'.$ext;
            $upload_path='complite/';
            $file_url=$upload_path.$file_full_name;
            $success=$file->move($upload_path,$file_full_name);
            $task['Task']=$file_url;

         // echo "<pre>";
         // print_r($task);
         // echo "</pre>";
         // die();
$data=DB::table('complite')->insert($task);

        if ($data) {
            $notification = array(
                'message' => 'Task Submit Successful!', 
                'alert-type' => 'success'
                );

            return Redirect()-> route('/tasks')->with($notification);
      }
      else{
         $notification = array(
                'message' => 'Task Submit Failed', 
                'alert-type' => 'error'
                );

            return Redirect()-> back()->with($notification);
      }
    }


           public function memberView($id)
    {
        // echo $id;
        // die();
        $data=DB::table('users')->where('id',$id)->first();
        return view('user/single', compact('data'));
    }



  public function reject(){
       $data=DB::table('tasks')->where('Complite',2)->get();

       return view('user/reject', compact('data'));
    }



     public function complite(){
       $data=DB::table('tasks')->where('Complite',1)->get();

       return view('user/complite', compact('data'));
    }



     public function stbmited(){
       // $data=DB::table('tasks')->get();
       $data=DB::table('tasks')->join('complite','tasks.id','complite.Task_Id')->select('tasks.*', 'complite.*' )->where('Complite',NULL)->get();

       return view('user/submited', compact('data'));
    }




}
