<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->name('home')->middleware('verified');
Route::get('/tasks','HomeController@task_view')->name('/tasks')->middleware('verified');
Route::get('tasks/{id}','HomeController@view')->name('tasks/{id}')->middleware('verified');
Route::get('/team_member','HomeController@team_member')->name('team_member')->middleware('verified');
Route::get('profile/','HomeController@Userprofile')->name('profile/')->middleware('verified');
Route::post('profile/','HomeController@profile_picture')->name('profile/');
Route::post('submit/task','HomeController@submit_task')->name('submit/task');
Route::get('download/task/{task}','HomeController@download')->name('download/{task}')->middleware('verified');
Route::get('member/{id}','HomeController@memberView')->name('member/{id}')->middleware('verified');

Route::get('task/submit','HomeController@stbmited')->name('task/submit');
Route::get('reject','HomeController@reject')->name('reject');
Route::get('complited','HomeController@complite')->name('complited');





Route::prefix('admin')->group(base_path('routes/admin.php'));
// Route::group(['prefix'=>'admin'],function() {
// 	Route::get('admin-login','Admin\LoginController@AdminLogin')->name('admin-login');
// 	Route::get('admin-login','Admin\LoginController@login')->name('admin.login');
//});


Route::get('/home', 'HomeController@index')->name('home');
