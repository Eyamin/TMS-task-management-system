<?php
	Route::get('admin-login','Admin\LoginController@AdminLogin')->name('admin-login');
	Route::post('admin-login','Admin\LoginController@login')->name('admin.login');
	Route::group(['middleware'=>'auth:admin'],function (){
		Route::get('admin_home','AdminController@adminindex')->name('admin_home');
		Route::get('upload/task','AdminController@task_form')->name('upload/task');
		Route::post('upload.task','AdminController@upload_task')->name('upload.task');
		Route::get('tasks','AdminController@task_view')->name('tasks');
		Route::get('tasks/{id}','AdminController@view')->name('tasks/{id}');
		Route::get('/edit/{id}','AdminController@edit');
		Route::post('/editdata/{id}','AdminController@editdata');
		Route::get('task/delete/{id}','AdminController@destroy_task');
		Route::get('allusers','AdminController@allusers')->name('allusers');
		Route::get('user/{id}','AdminController@userView')->name('user/{id}');
		Route::get('user/delete/{id}','AdminController@destroy_usders');
		Route::get('task/complite/{id}','AdminController@complite_task');
		Route::get('complite','AdminController@complite')->name('complite');

		Route::get('task/submit','AdminController@stbmited')->name('task/submit');
		Route::get('reject','AdminController@reject')->name('reject');
		Route::get('task/reject/{id}','AdminController@reject_task');
		Route::get('/task/complite/{Task}','AdminController@downloadtask')->name('/task/complite/{Task}');

});
?>