@extends('layouts.app')
@section('content')
<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
</head>
<body>
<h1 align="center" >Profile</h1>
<table align="center" class="rwd-table"id="myTable">
                  <tr>
                    <th></th>
                    <td>
                      <img src="img/{{Auth::user()->image}} ">
                    </td>
                  </tr>
                         <tr>
                          <th>Name</th>
                          <td data-th="Genre">{{ Auth::user()->name }}</td>
                        </tr>
                         <tr>
                          <th>Email</th>
                          <td data-th="Genre">{{ Auth::user()->email }}</td>
                        </tr>
                        <tr>
                    <form action="{{route('profile/')}}" method="POST" enctype="multipart/form-data">
                  @csrf
                          <th>Choose a Picture</th>
                          <td data-th="Genre"> <input name="image" type="file" class="form"> </td>
                        </tr>
                         <tr>
                            <th></th>
                          <td><input type="submit"value="Submit" class="submit" ></td>
                        </tr>
                      </form>
                         

</table>


<style type="text/css">
@import "https://fonts.googleapis.com/css?family=Montserrat:300,400,700";
.rwd-table {
  
  min-width: 300px;
}
.rwd-table tr {
  border-top: 1px solid #ddd;
  border-bottom: 1px solid #ddd;
}
.rwd-table th {
  display: none;
}
.rwd-table td {
  display: block;
}
.rwd-table td:first-child {
  padding-top: .5em;
}
.rwd-table td:last-child {
  padding-bottom: .5em;
}
.rwd-table td:before {
  content: attr(data-th) ": ";
  font-weight: bold;
  width: 6.5em;
  display: inline-block;
}
@media (min-width: 480px) {
  .rwd-table td:before {
    display: none;
  }
}
img{
  display: inline-block;
  border-top: 10px;
  border-bottom: none;
  width: 100px; height: 100px; 
  border-radius: 50%;
  margin-left: 15px;
  margin-top: 10px;
  float:center;

    }
.rwd-table th, .rwd-table td {
  text-align: left;
}
@media (min-width: 480px) {
  .rwd-table th, .rwd-table td {
    display: table-cell;
    padding: .25em .5em;
  }
  .rwd-table th:first-child, .rwd-table td:first-child {
    padding-left: 0;
  }
  .rwd-table th:last-child, .rwd-table td:last-child {
    padding-right: 0;
  }
}

body {
  padding: 0 2em;
  font-family: Montserrat, sans-serif;
  -webkit-font-smoothing: antialiased;
  text-rendering: optimizeLegibility;
  color: #444;
  background: #eee;
}

h1 {
  font-weight: normal;
  letter-spacing: -1px;
  color: #34495E;
}

.rwd-table {
  background: #34495E;
  color: #fff;
  border-radius: .4em;
  overflow: hidden;
}
.rwd-table tr {
  border-color: #46637f;
}
.rwd-table th, .rwd-table td {
  margin: .5em 1em;
}
@media (min-width: 480px) {
  .rwd-table th, .rwd-table td {
    padding: 1em !important;
  }
}
.rwd-table th, .rwd-table td:before {
  color: #dd5;
}


</style>

<script src="https:////cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
      <script type="text/javascript">
        $(document).ready( function () {
        $('#myTable').DataTable();
    } );
    </script>
    
  </body>
  </html>
  @endsection