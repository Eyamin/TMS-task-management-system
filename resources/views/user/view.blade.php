@extends('layouts.app')
@section('content')
<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
</head>
<body>
<h1 align="center" >Task View</h1>
<table align="center" class="rwd-table"id="myTable">

                          <tr>
                          <th><a class="clickme success" href="{{url('download/'.$data->Task)}}" role="button"dounload="">Dounload</a></th>
                          
                        </tr>
                        <tr>
                          <th>Title</th>
                          <td data-th="Genre">{{$data->Name}}</td>
                        </tr>
                         <tr>
                          <th>Category</th>
                          <td data-th="Genre">{{$data->Category}}</td>
                        </tr>
                         <tr>
                          <th>Description</th>
                          <td data-th="Genre">{{$data->Description}}</td>
                        </tr>
                         <tr>
                          <th>Upload Time</th>
                          <td data-th="Genre">{{$data->created_at}}</td>
                        </tr>
                        <tr>
                          <th>Description</th>
                          <td data-th="Genre">{{$data->Description}}</td>
                        </tr>
                         
                    <form action="{{route('submit/task')}}" method="POST" enctype="multipart/form-data">
                      @csrf
                      <tr>
                          <th>Submit Task</th>
                          <td data-th="Genre"><input name="Task" type="file" class="form-control"required></td>
                        </tr>


<tr>
  <td>
                    <input type="hidden" name="Task_Id" value="{{$data->id}}">

                    <div class="form-group">
                    <input type="submit"value="Submit" class="submit" >
                  </div>
                </tr>
              </form>

</table>


<style type="text/css">
@import "https://fonts.googleapis.com/css?family=Montserrat:300,400,700";
.rwd-table {
  
  min-width: 300px;
}
.rwd-table tr {
  border-top: 1px solid #ddd;
  border-bottom: 1px solid #ddd;
}
.rwd-table th {
  display: none;
}
.rwd-table td {
  display: block;
}
.rwd-table td:first-child {
  padding-top: .5em;
}
.rwd-table td:last-child {
  padding-bottom: .5em;
}
.rwd-table td:before {
  content: attr(data-th) ": ";
  font-weight: bold;
  width: 6.5em;
  display: inline-block;
}
@media (min-width: 480px) {
  .rwd-table td:before {
    display: none;
  }
}
.rwd-table th, .rwd-table td {
  text-align: left;
}
@media (min-width: 480px) {
  .rwd-table th, .rwd-table td {
    display: table-cell;
    padding: .25em .5em;
  }
  .rwd-table th:first-child, .rwd-table td:first-child {
    padding-left: 0;
  }
  .rwd-table th:last-child, .rwd-table td:last-child {
    padding-right: 0;
  }
}

body {
  padding: 0 2em;
  font-family: Montserrat, sans-serif;
  -webkit-font-smoothing: antialiased;
  text-rendering: optimizeLegibility;
  color: #444;
  background: #eee;
}

h1 {
  font-weight: normal;
  letter-spacing: -1px;
  color: #34495E;
}

.rwd-table {
  background: #34495E;
  color: #fff;
  border-radius: .4em;
  overflow: hidden;
}
.rwd-table tr {
  border-color: #46637f;
}
.rwd-table th, .rwd-table td {
  margin: .5em 1em;
}
@media (min-width: 480px) {
  .rwd-table th, .rwd-table td {
    padding: 1em !important;
  }
}
.rwd-table th, .rwd-table td:before {
  color: #dd5;
}
.submit{
  background-color: #184b75;
    border-top: 10px solid #4c8fc5;
    border-right: 18px solid #1a374e;
    border-bottom: 10px solid #4c8fc5;
    border-left: 18px solid #1e3f5a;width: 150px;color: #fff;
}
.clickme {
    background-color: #EEEEEE;
    padding: 8px 20px;
    text-decoration:none;
    font-weight:bold;
    border-radius:5px;
    cursor:pointer;
}



.success {
    background-color:#00EB89;
    color: #FFFFFF;
}

.success:hover {
    background-color:#00D77E;
    color: #FFFFFF;
}


</style>

<script src="https:////cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
      <script type="text/javascript">
        $(document).ready( function () {
        $('#myTable').DataTable();
    } );
    </script>
    
  </body>
  </html>
  @endsection