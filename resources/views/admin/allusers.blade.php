@extends('admin.master')
@section('content')
<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
      <script src="https:////cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
</head>
<body class="body">
<h1 align="center" class="rwd-table-h1" >Member</h1>
<table align="center" class="rwd-table"id="myTable">
        <tr class="rwd-table-tr">
           <th class="rwd-table-th">Name</th>
           <th class="rwd-table-th">Image</th>
         <th class="rwd-table-th">Email</th>
         <th class="rwd-table-th">Action</th>
        </tr>
  @foreach($data as $row)
  <tr class="rwd-table-tr">
    
    <td class="rwd-table-td" data-th="Genre">{{$row->name}}</td>
    <td class="rwd-table-td" data-th="Genre"><img src="../img/{{$row->image}}"></td>
    <td class="rwd-table-td" data-th="Genre">{{$row->email}}</td>
    <td class="rwd-table-td" data-th="Genre"><a class="clickme info" href="{{url('admin/user/'.$row->id)}}" role="button">View</a></td>
    <td class="rwd-table-td" data-th="Genre"><a class="clickme danger" href="{{url('admin/user/delete/'.$row->id)}}" role="button">Delete</a></td>

  </tr>
  @endforeach

</table>


<style type="text/css">
@import "https://fonts.googleapis.com/css?family=Montserrat:300,400,700";
.rwd-table {
  
  min-width: 300px;
}
.rwd-table-tr {
  border-top: 1px solid #ddd;
  border-bottom: 1px solid #ddd;
}
.rwd-table-th {
  display: none;
}
.rwd-table-td {
  display: block;
}
.rwd-table-td:first-child {
  padding-top: .5em;
}
.rwd-table-td:last-child {
  padding-bottom: .5em;
}
.rwd-table-td:before {
  content: attr(data-th) ": ";
  font-weight: bold;
  width: 6.5em;
  display: inline-block;
}
@media (min-width: 480px) {
  .rwd-table td:before {
    display: none;
  }
}

.rwd-table-th, .rwd-table-td {
  text-align: left;
}
@media (min-width: 480px) {
  .rwd-table-th, .rwd-table-td {
    display: table-cell;
    padding: .25em .5em;
  }
  .rwd-table-th:first-child, .rwd-table-td:first-child {
    padding-left: 0;
  }
  .rwd-table-th:last-child, .rwd-table-td:last-child {
    padding-right: 0;
  }
}
img{
  display: inline-block;
  border-top: 10px;
  border-bottom: none;
  width: 100px; height: 100px; 
  border-radius: 50%;
  margin-left: 15px;
  margin-top: 10px;
  float:center;

 }

.body {
  padding: 0 2em;
  font-family: Montserrat, sans-serif;
  -webkit-font-smoothing: antialiased;
  text-rendering: optimizeLegibility;
  color: #444;
  background: #eee;
}

.h1 {
  font-weight: normal;
  letter-spacing: -1px;
  color: #34495E;
}

.rwd-table {
  background: #34495E;
  color: #fff;
  border-radius: .4em;
  overflow: hidden;
}
.rwd-table-tr {
  border-color: #46637f;
}
.rwd-table-th, .rwd-table-td {
  margin: .5em 1em;
}
@media (min-width: 480px) {
  .rwd-table-th, .rwd-table-td {
    padding: 1em !important;
  }
}
.rwd-table-th, .rwd-table-td:before {
  color: #dd5;
}
.clickme {
    background-color: #EEEEEE;
    padding: 8px 20px;
    text-decoration:none;
    font-weight:bold;
    border-radius:5px;
    cursor:pointer;
}

.danger {
    background-color:#FF0040;
    color: #FFFFFF;
}

.danger:hover {
    background-color:#EB003B;
    color: #FFFFFF;
}

.success {
    background-color:#00EB89;
    color: #FFFFFF;
}

.success:hover {
    background-color:#00D77E;
    color: #FFFFFF;
}

.warning {
    background-color:#FFA500;
    color: #FFFFFF;
}

.warning:hover {
    background-color:#EB9800;
    color: #FFFFFF;
}

.info {
    background-color:#00D4FF;
    color: #FFFFFF;
}

.info:hover {
    background-color:#00C4EB;
    color: #FFFFFF;
}

/*.default {
    background-color:#D1D6DC;
    color: #000000;
}

.default:hover {
    background-color:#DCE0E5;
    color: #555555;
}*/

</style>



      <script type="text/javascript">
        $(document).ready( function () {
        $('#myTable').DataTable();
    } );
    </script>

    
    
  </body>
  </html>
  @endsection