 <title>TASK MANAGEMENT SYSTEM </title>


<ul>
  
  <li><a href="{{ route('allusers') }}">Member</a></li>

  <li class="dpdn"style="float:right;">
    <a href="javascript:void(0)" class="dropbtn">{{ Auth::user()->name }}</a>
    <div class="dropdown-con">
      <a href="{{ route('logout') }}"
      onclick="event.preventDefault();
          document.getElementById('logout-form').submit();"> Logout</a>

             <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                  @csrf
             </form>
    </div>
  </li>


  <li class="dpdn">
    <a href="javascript:void(0)" class="dropbtn">Task</a>
    <div class="dropdown-con">
      <a href="{{ route('upload/task') }}"> Add New Task</a>
      <a href="{{ route('tasks') }}"> Available Task</a>
      <a href="{{ route('complite') }}"> Complite</a>
      <a href="{{ route('task/submit') }}"> Submited</a>
      <a href="{{ route('reject') }}"> Rejected</a>
      


    </div>
  </li>

</ul>



<style>
    ul {
      list-style-type: none;
      margin: 0;
      padding: 0;
      overflow: hidden;
      background-color: #333;

    }

    li {
      float: left;
    }

    li a, .dropbtn {
      display: inline-block;
      color: white;
      text-align: center;
      padding: 14px 16px;
      text-decoration: none;

    }

    li a:hover, .dpdn:hover .dropbtn {
      background-color: #080808;

    }

    li.dpdn {
      display: inline-block;
      color:white;
    }

    .dropdown-con {
      display: none;
      position: absolute;
      background-color: #f9f9f9;
      min-width: 160px;
      box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
      z-index: 1;
    }

    .dropdown-con a {
      color: black;
      padding: 12px 16px;
      text-decoration: none;
      display: block;
      text-align: left;

    }

    .dropdown-con a:hover {background-color: white;}

    .dpdn:hover .dropdown-con {
      display: block;
    }
    body{
      margin: 0;
    font-family: Nunito,sans-serif;
    font-size: .9rem;
    font-weight: 400;
    line-height: 1.6;
    color: #212529;
    text-align: left;
    background-color: #f8fafc;
    }
</style>
        <main class="py-4">
            @yield('content')
        </main>
    </div>
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
      <script src="../js/jquery.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>


 <script>
           @if(Session::has('message'))
                          var type = "{{ Session::get('alert-type', 'info') }}";
                 switch(type){
                              case 'info':
                      toastr.info("{{ Session::get('message') }}");
                       break;
                              
                  case 'warning':
                      toastr.warning("{{ Session::get('message') }}");
                  break;

                  case 'success':
                     toastr.success("{{ Session::get('message') }}");
                      break;

                  case 'error':
                     toastr.error("{{ Session::get('message') }}");
                  break;
                  }
           @endif
 </script> 