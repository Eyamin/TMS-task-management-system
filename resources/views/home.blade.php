@extends('layouts.app')

@section('content')
<link href="{{ asset('css/app.css') }}" rel="stylesheet">
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Hello!</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <h3> Mr.{{ Auth::user()->name }},</h3>
                    <h2>WELCOME TO TASK MANAGEMENT SYSTEM</h2>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
